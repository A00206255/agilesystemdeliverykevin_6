import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeliveryPersonAccessGUI {

	JFrame frame;
	private Database db = new Database();
	private Docket dock;
	
	public DeliveryPersonAccessGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 485, 359);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDeliveryPerson = new JLabel("Delivery Person Menu");
		lblDeliveryPerson.setFont(new Font("Yu Gothic UI Semilight", Font.BOLD, 22));
		lblDeliveryPerson.setBounds(125, 13, 287, 56);
		frame.getContentPane().add(lblDeliveryPerson);
		
		JButton btnDPA = new JButton("Search Delivery Docket");
		btnDPA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				DocketSearch ds =new DocketSearch("Search Delivery Dockets",db, dock);
				ds.frame.setVisible(true);
				frame.setVisible(false); 
				frame.dispose();								
			}
		});
		btnDPA.setFont(new Font("Yu Gothic UI Semilight", Font.BOLD, 17));
		btnDPA.setBounds(113, 124, 239, 56);
		frame.getContentPane().add(btnDPA);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LOGIN log = new LOGIN("Login");
				//Code to close current window
				frame.setVisible(false); 
				frame.dispose(); 
			}
		});
		btnBack.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 17));
		btnBack.setBounds(173, 235, 132, 39);
		frame.getContentPane().add(btnBack);
	}
}
