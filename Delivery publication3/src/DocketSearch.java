import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.ScrollPaneConstants;

public class DocketSearch {

	JFrame frame;
	private JTextField textField;

	private Database dbase = null;
	private Docket sc=null;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public DocketSearch(String s,Database d,Docket sub) {
		super();
		dbase=d;
		initialize();
		sc=sub;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 782, 586);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.BLACK);
		panel.setBorder(BorderFactory.createTitledBorder("Search Dockets"));
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(39, 28, 574, 445);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(55, 49, 419, 168);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblSearchByDocket = new JLabel("Search By Docket ID:                 ");
		lblSearchByDocket.setBounds(37, 5, 344, 18);
		panel_1.add(lblSearchByDocket);
		
		textField = new JTextField(10);
		textField.setBounds(37, 44, 251, 24);
		panel_1.add(textField);
		
		JButton button = new JButton("Search");
		button.setBounds(69, 94, 167, 43);
		panel_1.add(button);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBounds(55, 230, 419, 146);
		panel.add(panel_2);
		
		JLabel lblSearchByCustomer = new JLabel("Search By Customer ID:                 ");
		lblSearchByCustomer.setBounds(37, 5, 344, 18);
		panel_2.add(lblSearchByCustomer);
		
		textField_1 = new JTextField(10);
		textField_1.setBounds(37, 52, 251, 24);
		panel_2.add(textField_1);
		
		JButton button_1 = new JButton("Search");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Docket sc=new Docket(dbase);

				try 
				{
					ResultSet rs;
					rs= sc.searchDocket_CustID(Integer.parseInt(textField_1.getText()));
					String[] sub_details = new String[4];

					if(rs != null)
					{
						while(rs.next())
						{						
							sub_details[0]="Docket ID: "+rs.getString("Docket_ID");
							sub_details[1]="Delivery Area: "+rs.getString("Delivery_Area");
							sub_details[2]="DelPerson ID: "+rs.getString("DelPerson_ID");
							sub_details[3]="Customer ID: "+rs.getString("Cust_ID");
						}
						
						String str="";
						
						for(int i=0;i<4;i++) 
						{
							System.out.println(sub_details[i]);
							str+=sub_details[i]+"\n";
						}
						JOptionPane.showMessageDialog(null, str);						
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Customer ID not found");							
					}
					
					
				}
				catch (NumberFormatException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_1.setBounds(69, 86, 167, 37);
		panel_2.add(button_1);
		
		JButton button_2 = new JButton("Cancel");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				//Code to open the main menu			
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false); 
				frame.dispose(); 			}
		});
		button_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		button_2.setBounds(177, 389, 178, 40);
		panel.add(button_2);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
		
				Docket sc=new Docket(dbase);
				try 
				{
					ResultSet rs;
					
					rs= sc.searchDocket_DocketID(Integer.parseInt(textField.getText()));
					String[] sub_details = new String[4];
					//System.out.println(rs);
					
					if(rs != null)
					{						
						while(rs.next())
						{						
							sub_details[0]="Docket ID: "+rs.getString("Docket_ID");
							sub_details[1]="Delivery Area: "+rs.getString("Delivery_Area");
							sub_details[2]="DelPerson ID: "+rs.getString("DelPerson_ID");
							sub_details[3]="Customer ID: "+rs.getString("Cust_ID");
						}
						
						String str="";
						for(int i=0;i<4;i++) 
						{
							System.out.println(sub_details[i]);
							str+=sub_details[i]+"\n";
						}
						
						JOptionPane.showMessageDialog(null, str);
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Docket ID not found");							
					}
				
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		});
	}
}
