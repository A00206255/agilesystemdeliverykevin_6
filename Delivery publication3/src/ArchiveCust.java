

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class ArchiveCust {

	JFrame frame;
	private JTextField textField;
	private Database db;
	private Customers cust;

	/**
	 * Create the application.
	 * @param cust 
	 * @param db 
	 */
	public ArchiveCust(Database d, Customers c) {
		db = d;
		cust = c;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 528, 421);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setForeground(Color.BLACK);
		panel.setBorder(BorderFactory.createTitledBorder("Archive Customer"));
		panel.setBackground(Color.LIGHT_GRAY);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		JButton button_1 = new JButton("Cancel");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				//Code to open the main menu			
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false); 
				frame.dispose(); 				
			}
		});
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		button_1.setBounds(337, 300, 125, 27);
		panel.add(button_1);
		
		JButton btnArchiveCustId = new JButton("Archive Cust ID");
		btnArchiveCustId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				int id = Integer.parseInt(textField.getText());
				boolean archive = false;
				
				if(id > 0)
				{
					Customers c =new Customers(db);
					try 
					{
						archive = c.archiveCustomer(id);
						
						if (archive == true)
						{
							JOptionPane.showMessageDialog(null, "Archiving Complete");
							textField.setText("");
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Customer ID does not exist");	
							textField.setText("");						
						}
					}
					catch (SQLException e1) 
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			
			}
		});
		btnArchiveCustId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnArchiveCustId.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnArchiveCustId.setBounds(70, 300, 159, 27);
		panel.add(btnArchiveCustId);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBorder(new LineBorder(new Color(171, 173, 179), 2, true));
		textField.setBounds(255, 172, 139, 33);
		panel.add(textField);
		
		JLabel lblCistUd = new JLabel("Cust ID:");
		lblCistUd.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCistUd.setBounds(137, 173, 106, 26);
		panel.add(lblCistUd);
		
		JLabel lblEnterTheId = new JLabel("Enter the ID of the customer account you wish to archive");
		lblEnterTheId.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lblEnterTheId.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblEnterTheId.setBounds(70, 49, 419, 41);
		panel.add(lblEnterTheId);
	}
}
